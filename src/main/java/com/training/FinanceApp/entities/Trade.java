package com.training.FinanceApp.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.training.FinanceApp.Converter.IdToString;
import com.training.FinanceApp.Converter.StringToId;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Trade {
    public enum Status {
        CREATED, PROCESSING, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLYFILLED, ERROR
    }
    public enum ActionType{
        BUY,
        SELL
    }
   Date today = Calendar.getInstance().getTime();

    @Id
    @JsonSerialize(converter = IdToString.class)
    @JsonDeserialize(converter = StringToId.class)
    private ObjectId objectId;
    private Calendar dateCreated = dateToCalendar(today);
    private Status status = Status.CREATED;
    private String ticker = "";
    private int quantity = 0;
    private double requestedPrice = 0.0;
    private ActionType actionType;
    private double totalValue = 0.0;

    
    public Trade(Calendar dateCreated, String ticker, int quantity, double requestedPrice, Status status, ActionType actionType, double totalValue) {
        this.dateCreated = dateCreated;
        this.ticker = ticker;
        this.quantity = quantity;
        this.requestedPrice = requestedPrice;
        this.status = status;
        this.actionType = actionType;
        this.totalValue = totalValue;
    }

    public static Calendar dateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public Trade() {
    }

    public Calendar getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Calendar dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }


}