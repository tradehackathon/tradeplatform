package com.training.FinanceApp.entities;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.training.FinanceApp.Converter.IdToString;
import com.training.FinanceApp.Converter.StringToId;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Portfolio {
    
    @Id
    @JsonSerialize(converter = IdToString.class)
    @JsonDeserialize(converter = StringToId.class)
    private ObjectId objectId;
    private ArrayList<Trade> trades = new ArrayList<Trade>();
    private double netValue = 0.0;
    private double netCashBalance = 10000;


    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public ArrayList<Trade> getTrades() {
        return trades;
    }

    public void setTrades(ArrayList<Trade> trades) {
        this.trades = trades;
    }

    public double getNetValue() {
        return netValue;
    }

    public void setNetValue() {
        for (Trade trade : trades) {
            this.netValue += trade.getTotalValue();
        }
    }

    public void setNetValue(double netValue) {
        this.netValue = netValue;
    }

    public double getNetCashBalance() {
        return netCashBalance;
    }

    public void setNetCashBalance(double netCashBalance) {
        this.netCashBalance = netCashBalance;
    }
}
