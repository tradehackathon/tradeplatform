package com.training.FinanceApp.service;

import java.util.Collection;
import java.util.Optional;

import com.training.FinanceApp.entities.Portfolio;
import com.training.FinanceApp.entities.Trade;

import org.bson.types.ObjectId;

public interface PortfolioService {
    
    public Collection<Portfolio> getAllStocks();
    public void addPortfolio(Portfolio port);

    public Portfolio addOrUpdateTrade(Trade trade, ObjectId id);
	public Optional<Portfolio> getPortfolioById(ObjectId objectId);
    public Portfolio getOnePortfolio();
    public Trade buyOrder(Trade foundTrade, Trade newTrade, double netCashBalance);
    public double calculateLiveValue();
}
