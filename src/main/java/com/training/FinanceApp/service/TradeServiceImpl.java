package com.training.FinanceApp.service;

import java.lang.StackWalker.Option;
import java.util.Collection;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.training.FinanceApp.data.PortfolioRepo;
import com.training.FinanceApp.data.TradeRepo;
import com.training.FinanceApp.entities.Trade;
import com.training.FinanceApp.entities.Trade.Status;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;

import ch.qos.logback.core.joran.conditional.ElseAction;

@Service
public class TradeServiceImpl implements TradeService {
    
    @Autowired
    private TradeRepo tradeRepo;

    @Override
    public Collection<Trade> getAllTrades() {
        return tradeRepo.findAll(Sort.by(Sort.Direction.DESC, "dateCreated")).stream()
            .collect(Collectors.toList());
    }

    @Override
    public Trade addTrade(Trade trade) {
        System.out.println("TRADE ADDED IN SERVICE");
        return tradeRepo.insert(trade);
    }

    @Override
    public void deleteTradeById(ObjectId objectId) {
        tradeRepo.deleteById(objectId);
    }

    @Override
    public Optional<Trade> getTradeById(ObjectId objectId) {
        return tradeRepo.findById(objectId);
    }

    @Override
    public Trade updateTradeById(ObjectId id, Trade trade) {
        // TODO Auto-generated method stub
        if(tradeRepo.findById(id) != null){
            
            Trade currentTrade = tradeRepo.findById(id).get();

            if(currentTrade.getStatus() == Status.CREATED){
                currentTrade.setStatus(trade.getStatus());
                currentTrade.setQuantity(trade.getQuantity());
                currentTrade.setRequestedPrice(trade.getRequestedPrice());
                currentTrade.setTicker(trade.getTicker());
                System.out.println("Trade updated");

                return tradeRepo.save(currentTrade);
            }else{
                System.out.println("status:  " + currentTrade.getStatus() + " could not be updated");
                return null;
            }   
        }else{
            System.out.println("not an existing trade");
            return null;
            
            
        } 
    }

   


    
}