package com.training.FinanceApp.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import com.training.FinanceApp.data.PortfolioRepo;
import com.training.FinanceApp.data.PortfolioRepo;
import com.training.FinanceApp.entities.Portfolio;
import com.training.FinanceApp.entities.Trade;
import com.training.FinanceApp.entities.Trade.ActionType;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;

@Service
public class PortfolioServiceImpl implements PortfolioService {

    @Autowired
    private PortfolioRepo portfolioRepo;

    @Override
    public Collection<Portfolio> getAllStocks() {
        return portfolioRepo.findAll().stream().collect(Collectors.toList());
    }

    @Override
    public void addPortfolio(Portfolio port) {
        portfolioRepo.insert(port);

    }

    @Override
    public Portfolio addOrUpdateTrade(Trade newTrade, ObjectId id) {
        // TODO Auto-generated method stub
        System.out.println("addtrade IN SERVICE " + newTrade.getStatus());

        if (portfolioRepo.findById(id) != null) {
            Portfolio portfolio = portfolioRepo.findById(id).get();
            ArrayList<Trade> trades = portfolio.getTrades();
            double netCashBalance = portfolio.getNetCashBalance();

            //if no trades in portfolio exist, add it
           if(trades.isEmpty()){               
                //Perform buy actions
                
                newTrade = buyOrder(null, newTrade, netCashBalance);
                
                if (newTrade != null) {
                    System.out.println("performing add trade and buy order");
                    System.out.println("newTrade value: " + newTrade.getTotalValue());
                    trades.add(newTrade);
                    //Subtracting from netCashBalance
                    netCashBalance -= newTrade.getTotalValue();  
                    System.out.println("Updated new: " + netCashBalance);   
                }
           }
           else { //if it exists
                //try to find the trade
                int indexOfTrade = -1;
                Trade foundTrade = null;
                for (int i = 0; i < trades.size(); i++) {
                    if (trades.get(i).getTicker().equals(newTrade.getTicker())) {
                        indexOfTrade = i;
                        foundTrade = trades.get(i);
                        // have to manually update actiontype of trade in portfolio to the actiontype of incoming trade
                        foundTrade.setActionType(newTrade.getActionType());
                        break;
                    }
                }

                if (indexOfTrade == -1 && newTrade.getActionType().equals(Trade.ActionType.BUY)) {

                    //Perform buy action
                    newTrade = buyOrder(null, newTrade, netCashBalance);
                    if (newTrade != null) {
                        trades.add(newTrade);
                        //Subtracting from netCashBalance
                        netCashBalance -= newTrade.getTotalValue();     
                    }
                } 
                else if (indexOfTrade == -1 && newTrade.getActionType().equals(Trade.ActionType.SELL)) {
                    System.out.println("Cannot sell stock, you own 0 shares");
                }
                else {
                    // Trade exists in list, update it

                    switch(foundTrade.getActionType()){
                        case BUY: //BUY order
                            newTrade = buyOrder(foundTrade, newTrade, netCashBalance);
                            if (newTrade != null) {
                                trades.set(indexOfTrade, newTrade);
                                //Subtracting from netCashBalance
                                netCashBalance -= newTrade.getTotalValue();     
                            }

                            break;
                        case SELL: //SELL order
                            //Check if the user has enough shares to sell
                            //Decrease quantity if successful
                            if((newTrade.getQuantity() >  foundTrade.getQuantity())){ //Invalid sell
                                System.out.println("New sell quantity is greater than existing quantity");
                            }
                            else { //Valid sell. Reduce quantity, update average price and add to cash
                                if(foundTrade.getQuantity() == newTrade.getQuantity()){ //Selling all shares                                    
                                    netCashBalance += newTrade.getTotalValue();
                                    //Remove from trades list because quantity is zero
                                    trades.remove(indexOfTrade);
                                    
                                }
                                else{ //Selling some shares
                                    netCashBalance += newTrade.getTotalValue();
                                    foundTrade.setQuantity(foundTrade.getQuantity() - newTrade.getQuantity());
                                    foundTrade.setTotalValue(foundTrade.getQuantity() * foundTrade.getRequestedPrice());
                                    trades.set(indexOfTrade, foundTrade); //Update trade object in list with new quantity
                                }
                            }
                             
                            break;
                    }
                }
           }
           
            portfolio.setTrades(trades);
            portfolio.setNetCashBalance(netCashBalance);
            portfolioRepo.save(portfolio);

            System.out.println(trades.size());

        }
        //Should it be return null?
        return null;
    }

    @Override
    public Optional<Portfolio> getPortfolioById(ObjectId objectId) {
        // TODO Auto-generated method stub
        return portfolioRepo.findById(objectId);
    }

    @Override
    public  Portfolio getOnePortfolio() {
        // TODO Auto-generated method stub
        return portfolioRepo.findAll().get(0);
    }

    @Override
    public Trade buyOrder(Trade foundTrade, Trade newTrade, double netCashBalance) {
        System.out.println("Inside buy order");

        double totalPrice = 0;
        double averagePrice = 0;

        // Check if they have enough cash to buy
        if (netCashBalance > newTrade.getTotalValue()) {
        
            if(foundTrade != null) {
                // Add to the quantity and update to average price
                newTrade.setQuantity(newTrade.getQuantity() + foundTrade.getQuantity());
                totalPrice = newTrade.getTotalValue() + foundTrade.getTotalValue();
                averagePrice = totalPrice / newTrade.getQuantity();
                newTrade.setRequestedPrice(averagePrice);
                newTrade.setTotalValue(averagePrice * newTrade.getQuantity());
            }
                            
        } else { // Not enough money to buy
            System.out.println("Not enough cash to buy");
            return null;
        }
        return newTrade;
    }

    @Override
    public double calculateLiveValue() {
        Portfolio portfolio = portfolioRepo.findAll().get(0);
        ArrayList<Trade> trades = portfolio.getTrades();
        for (Trade trade : trades) {
            
        }
        return 0;
    }

}
