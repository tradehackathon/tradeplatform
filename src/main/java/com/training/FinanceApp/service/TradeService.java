package com.training.FinanceApp.service;

import java.util.Collection;
import java.util.Optional;


import com.training.FinanceApp.entities.Trade;

import org.bson.types.ObjectId;


public interface TradeService {
    
    public Collection<Trade> getAllTrades();
    public Trade addTrade(Trade trade);

    public void deleteTradeById(ObjectId objectId);

	public Optional<Trade> getTradeById(ObjectId objectId);
	public Trade updateTradeById(ObjectId id, Trade trade);
   

}