package com.training.FinanceApp;

import java.util.stream.Stream;

import com.training.FinanceApp.data.TradeRepo;
import com.training.FinanceApp.entities.Trade;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class FinanceAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinanceAppApplication.class, args);
	}
/*
	@Bean
    ApplicationRunner init(TradeRepo traderepo) {
        return args -> {
            Stream.of("ABB", "GGG", "CCC").forEach(name -> {
                Trade trade = new Trade();
                trade.setTicker(name);
                traderepo.save(trade);
            });
            traderepo.findAll().forEach(System.out::println);
        };
    }
*/


}
