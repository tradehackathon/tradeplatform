package com.training.FinanceApp.Converter;

import com.fasterxml.jackson.databind.util.StdConverter;

import org.bson.types.ObjectId;

public class StringToId extends StdConverter<String, ObjectId> {

    @Override
    public ObjectId convert(String ID) {
        // TODO Auto-generated method stub
        return new ObjectId(ID);
    }
}