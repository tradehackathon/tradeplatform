package com.training.FinanceApp.Converter;

import com.fasterxml.jackson.databind.util.StdConverter;

import org.bson.types.ObjectId;

public class IdToString extends StdConverter<ObjectId, String> {

    @Override
    public String convert(ObjectId ID) {
        // TODO Auto-generated method stub
        return ID.toString();
    }
   
}