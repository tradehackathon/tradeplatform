package com.training.FinanceApp.controller;

import java.security.Provider.Service;
import java.util.Collection;
import java.util.Optional;

import com.training.FinanceApp.entities.Portfolio;
import com.training.FinanceApp.entities.Trade;
import com.training.FinanceApp.service.PortfolioService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/portfolio")
@CrossOrigin(origins = "http://localhost:4200")
public class PortfolioController {

    @Autowired
    private PortfolioService portService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Portfolio> getAllStocks() {
        return portService.getAllStocks();
    }

    @RequestMapping(method=RequestMethod.POST)
    public void addPortfolio(@RequestBody Portfolio port) {
        portService.addPortfolio(port);
    }

    @RequestMapping(value ="/{id}", method=RequestMethod.PUT)
    public Portfolio addTradeToPortfolio(@RequestBody Trade trade, @PathVariable("id") String id) {
        System.out.println("addtradetoportfolio" + trade.getTicker());
        return portService.addOrUpdateTrade(trade, new ObjectId(id));
     }


     @RequestMapping(method = RequestMethod.GET, value = "/{id}")
     public Optional<Portfolio> getPortfolioById(@PathVariable("id") String id) {
        Optional<Portfolio> portfolio = portService.getPortfolioById(new ObjectId(id));
         if(!portfolio.isPresent()){
             throw new ResponseStatusException(HttpStatus.NOT_FOUND);
         }
         return portfolio;
     }

     
     @RequestMapping(method = RequestMethod.GET, value = "/1")
     public Portfolio getOnePortfolio() {
        return  portService.getOnePortfolio();
     }
 

}
