package com.training.FinanceApp.controller;

import java.security.Provider.Service;
import java.util.Collection;
import java.util.Optional;

import com.training.FinanceApp.entities.Trade;
import com.training.FinanceApp.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/trades")
@CrossOrigin(origins = "http://localhost:4200")
public class TradeController {
    
    @Autowired
    private TradeService tradeService;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Trade> getAllTrades() {
        return tradeService.getAllTrades();
    }

    @RequestMapping(method=RequestMethod.POST)
    public Trade addTrade(@RequestBody Trade trade) {
        return tradeService.addTrade(trade);
    }

    @RequestMapping(value ="/{id}", method=RequestMethod.DELETE)
    public void deleteTrade(@PathVariable("id") ObjectId id) {
        tradeService.deleteTradeById(id);
    }
    
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public Optional<Trade> getTradeById(@PathVariable("id") String id) {
        Optional<Trade> trade = tradeService.getTradeById(new ObjectId(id));
        if(!trade.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
		return trade;
    }

  

    @RequestMapping(value ="/{id}", method=RequestMethod.PUT)
    public Trade updateTrade(@RequestBody Trade trade, @PathVariable("id") ObjectId id) {
        return tradeService.updateTradeById(id, trade);
     }
}