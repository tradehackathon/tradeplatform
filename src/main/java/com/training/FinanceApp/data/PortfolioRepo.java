package com.training.FinanceApp.data;

import java.util.Optional;

import com.training.FinanceApp.entities.Portfolio;
import com.training.FinanceApp.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
public interface PortfolioRepo extends MongoRepository<Portfolio, ObjectId> {

}
