import { Trade } from "./trade";

export interface Portfolio{
    trades: Array<Trade>;
    netValue: number;
    netCashBalance: number;
    
}