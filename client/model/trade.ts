export interface Trade{
    id:string;
    ticker: string;
    quantity: number;
    totalValue: number;
    requestedPrice:number;
    livePrice?: number;
}
