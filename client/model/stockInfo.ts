export interface StockInfo{
    country: [''];
    currency: [''];
    exchange: [''];
    finnhubIndustry: [''];
    ipo: [''];
    logo: [''];
    marketCapitalization: number;
    name: [''];
    phone:[''];
    shareOutstanding: number;
    ticker:[''];
    weburl:[''];
}