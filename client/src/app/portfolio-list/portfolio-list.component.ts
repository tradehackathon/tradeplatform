import { Component, OnInit } from '@angular/core';
import { PortfolioService} from '../portfolio/portfolio.service';
import { Portfolio } from 'model/portfolio';
import { interval } from 'rxjs';
import { Trade } from 'model/trade';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stock } from 'model/stock';
import { tap } from 'rxjs/operators';
import * as CanvasJS from './canvasjs.min';

@Component({
  selector: 'app-portfolio-list',
  templateUrl: './portfolio-list.component.html',
  styleUrls: ['./portfolio-list.component.css']
})
export class PortfolioListComponent implements OnInit {
  httpClient: any;

  constructor(private portfolioService: PortfolioService, httpClient: HttpClient) { }

  portfolioEmpty: Boolean;

  x_values: any = [];
  y_values: any = [];
  dataPoints: any = [];

  portfolios : Array<Portfolio>;
  netValue : number;

  price: number;
  eachValue: number;
  stock: Stock;
  netValueStr: String;
  livePriceStr: String;
  avgValueStr: String;

  private financeURL = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
  private defaultNumDays='&num_days';

  ngOnInit() {
    this.checkIfPortfolioExists();
    this.reloadData();

  }

  
  renderPieChart(){
   
    var k = document.getElementById("chartPieContainer");
    console.log(k.className);
    this.portfolioService.getOne().subscribe(res => {

      this.x_values = [];
      this.y_values = [];
      this.dataPoints = [];

      
      console.log(res);
     
      for(let i of res.trades){
        console.log(i);
          this.x_values.push(i.ticker);
          this.y_values.push(i.quantity);
      }
      console.log(this.x_values);
      console.log(this.y_values);

      for(var temp = 0; temp < this.x_values.length; temp++){
        this.dataPoints.push({indexLabel: this.x_values[temp], y: this.y_values[temp]})
        console.log(this.dataPoints[temp])
     }

     console.log( this.dataPoints);

    })


    let chart = new CanvasJS.Chart("chartPieContainer", {
     
      exportEnabled: true,
      backgroundColor: null,
      theme: "light2",

      title: {
        text: "Portfolio Diversity",
        titleFontColor: "white",
        labelFontColor:"white",
        fontColor:"white",
      },
      legend:{
        fontColor:"white",
      },
      data: [{
        type: "pie",
        toolTipContent: "<b>{indexLabel}</b>: {y}",
        showInLegend: "true",
        legendText: "{indexLabel}",
        indexLabelFontSize: 16,
        indexLabel: "{indexLabel} - {y}",
        indexLabelFontColor: "white",
        dataPoints: this.dataPoints
      }]
    });

    chart.render();

  
  }


  checkIfPortfolioExists(){

    this.portfolioService.getOneCheck().subscribe(data => {
        if(data.length == 0){
          this.portfolioService.createOne().subscribe(data => {
            console.log(data);
          });
        }
    });

   
  }

  reloadData() {
    this.portfolioService.getAll().subscribe(data => {
      this.portfolios = data;
      this.calculateLiveValue();
      this.renderPieChart();
    });

  

  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  index: number;
  temp: number;

  calculateLiveValue(): number {
    this.temp = 0;
    //interval(20000).subscribe ( x => {
      // console.log(this.portfolios)
      this.portfolios.forEach(portfolio => {
        portfolio.trades.forEach(trade => {
          //use api to get ticker live price

          this.portfolioService.getFinanceData(trade.ticker, 1).subscribe(res => {
            this.index = portfolio.trades.findIndex(t => t.ticker == trade.ticker);
            //console.log(this.index);

            this.livePriceStr = res.price_data[0][1].toFixed(2);

            //this.portfolios[0].trades[this.index].livePrice = res.price_data[0][1];
            //console.log(this.portfolios[0].trades[this.index].livePrice);
            
            //this.livePriceStr = this.portfolios[0].trades[this.index].livePrice.toFixed(2);
            this.portfolios[0].trades[this.index].livePrice = +this.livePriceStr;

            //Change average price 2 decimals
            // this.avgValueStr = this.portfolios[0].trades[this.index].requestedPrice.toFixed(2);
            // this.portfolios[0].trades[this.index].requestedPrice = +this.avgValueStr;

            //Calculate net live value of portfolio
            this.price = this.portfolios[0].trades[this.index].livePrice;
            this.eachValue = this.price * trade.quantity;
            this.temp = this.temp + this.eachValue;
            // this.netValue = this.netValue + this.eachValue;
            this.netValue = this.temp;

            //Limit to 2 decimals
            this.netValueStr = this.netValue.toFixed(2);
            this.netValue = +this.netValueStr;

          });
        });
      });

    //});
    
    return 0;
  }
}
