import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { TradeListComponent } from '../trade-list/trade-list.component';
import { Trade } from 'model/trade';
import { Stock } from 'model/stock';
import { StockInfo } from 'model/stockInfo';
import { Peers } from 'model/peers';

@Injectable({
  providedIn: 'root'
})

export class RequestService {
  private apiURL = 'http://docker8.conygre.com:8080';
  private financeURL = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
  private defaultNumDays='&num_days=';
   

  constructor(private httpClient: HttpClient) {
   
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  


  createTrade(trade): Observable<Trade> {
    console.log("CREATETRADE: " + JSON.stringify(trade));
    return this.httpClient.post<Trade>(this.apiURL + '/trades', JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

    handleError(error) {
      let errorMessage = '';
      if(error.error instanceof ErrorEvent) {
        // Get client-side error
        errorMessage = error.error.message;
      } else {
        // Get server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      window.alert(errorMessage);
      return throwError(errorMessage);
   }

   
   getFinanceData(ticker, days):Observable<any> {
    return this.httpClient.get<Stock>(this.financeURL + '?ticker=' + ticker + this.defaultNumDays + days);
   }

   httpOptionsGet = {
    headers: new HttpHeaders({
     
    })
    }  

   getFinData(ticker):Observable<any> {
      return this.httpClient.get<StockInfo>("https://finnhub.io/api/v1/stock/profile2?symbol=" + ticker + "&token=bth6nrn48v6v983bg2i0", this.httpOptionsGet)
   }

   getFX(base): Observable<any> {
      return this.httpClient.get("https://finnhub.io/api/v1/forex/rates?base=" + base + "&token=bth6nrn48v6v983bg2i0");
   }

   getPeer(ticker): Observable<any> {
     return this.httpClient.get<Peers>("https://finnhub.io/api/v1/stock/peers?symbol=" + ticker +"&token=bth6nrn48v6v983bg2i0");
   }
  
}
