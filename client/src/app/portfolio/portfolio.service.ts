import { Injectable } from '@angular/core';
import { Observable, throwError, timer } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trade } from 'model/trade';
import { retry, catchError, flatMap } from 'rxjs/operators';
import { interval } from 'rxjs';
import { Portfolio } from 'model/portfolio';
import { Stock } from 'model/stock';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService{
  
  constructor(private http: HttpClient) { }
  private apiURL = 'http://localhost:8080';

  private financeURL = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
  private defaultNumDays='&num_days';
  
  portfolio : Observable<Portfolio>;
  trades : Array<Trade>;
  
  getAll(): Observable<any> {
    return timer(10,5000).pipe(flatMap( () => {
      return this.http.get('http://localhost:8080/portfolio');
    }));
  }

  getLive(): Observable<any> {
    return timer(10,50000).pipe(flatMap( () => {
      return this.http.get('http://localhost:8080/portfolio');
    }));
  }

  getOneCheck(): Observable<any> {
      return this.http.get('http://localhost:8080/portfolio');
  }

  getOne(): Observable<any> {
      return this.http.get('http://localhost:8080/portfolio/1');
  }

  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  createOne(): Observable<any>{
    return this.http.post(this.apiURL + '/portfolio', {},this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getFinanceData(ticker, days):Observable<any> {
    return this.http.get<Stock>(this.financeURL + '?ticker=' + ticker + this.defaultNumDays + days);
  }

  
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }

}
