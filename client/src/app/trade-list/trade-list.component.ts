import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { TradeService } from '../trade/trade.service';
import {Sort} from '@angular/material/sort';
import { Observable } from 'rxjs/internal/Observable';
import { interval } from 'rxjs';

@Component({
  selector: 'app-trade-list',
  templateUrl: './trade-list.component.html',
  styleUrls: ['./trade-list.component.css']
})
export class TradeListComponent implements OnInit {

  trades:Array<any>;

  
 

  constructor(private tradeService: TradeService) { }
 
  ngOnInit() {
    this.reloadData();
   
  }

  reloadData() {
    this.tradeService.getAll().subscribe(data => {
      this.trades = data;
    });
  }

}
