import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RequestService } from '../request/request.service';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Portfolio } from 'model/portfolio';
import { PortfolioService } from '../portfolio/portfolio.service';
import { TradeService } from '../trade/trade.service';
import * as CanvasJS from './canvasjs.min';


@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.css']
})
export class RequestFormComponent implements OnInit{

  stocksInfoPopulated: Boolean;
  peersPopulated: Boolean;
  priceStrPopulated: Boolean;


  canvasChart:any;
  tickerName: String;
  compare_TickerName:string;

  stockInfo:any;
  peers:Array<any>;

  tradeForm: FormGroup;
  data:any = [];
  priceStr: String;

  x_values: any = [];
  y_values: any = [];

  compare_x_values: any = [];
  compare_y_values: any = [];
  isValid: boolean;

  dataPoints: any = [];
  compare_DataPoints: any [];

  constructor(
    public fb: FormBuilder,
    private router: Router,
    public restService: RequestService,
    public portfolioService: PortfolioService,
    public tradeService: TradeService,
    private http: HttpClient

  ){}

  ngOnInit(): void {

    this.tradeForm = this.fb.group({
      ticker: [''],
      quantity: [''],
      actionType: ['']
    })

    this.stocksInfoPopulated = false;
    this.peersPopulated = false;
    this.priceStrPopulated = false;
    this.canvasChart = 1;

    this.renderChart();

  }


  onSubmit(){

    const dataForSubmit = this.tradeForm.value;
    this.restService.getFinanceData(this.tradeForm.get('ticker').value, 1).subscribe(res => {
      
      this.priceStr = res.price_data[0][1].toFixed(2);
      dataForSubmit.requestedPrice =  +this.priceStr;
      dataForSubmit.totalValue = this.tradeForm.get('quantity').value * +this.priceStr;
    
    
      console.log( dataForSubmit.totalValue);

      this.restService.createTrade(dataForSubmit).subscribe(res => {
        console.log("tradeCreated");
        console.log(res);
  
        this.tradeForm.reset();

      })
    })

    this.onAnalyzeCompare(1, this.tradeForm.get('ticker').value)
    this.renderChart();
  
  }


  onAnalyzeCompare(chartNum, tickerChosen){

    console.log("calling on Analyze Compare");
    this.canvasChart = chartNum;
    if(chartNum == 1){
      this.tickerName = this.tradeForm.get('ticker').value;
      console.log(this.canvasChart + " " + this.tickerName );
      
     
    }else{
      this.compare_TickerName = tickerChosen;
      console.log( this.canvasChart + " " + this.compare_TickerName );
    }
    
    this.onAnalyze();

  }

  onAnalyze(){
   
    var moreTickerInfo = document.getElementById("moreTickerInfo");
    moreTickerInfo.style.display = "block";
    moreTickerInfo.style.visibility = "visible";
    console.log("analyze");

    var chart1 = document.getElementById("chartContainer1");
    var chart2 = document.getElementById("chartContainer2");
   

    if(this.canvasChart == 1){
      chart2.style.display = "none";
      chart1.style.display = "block";


      this.x_values = [];
      this.y_values = [];
      this.data = [];
      this.dataPoints = [];

    this.restService.getFinData(this.tickerName).subscribe(res => {
      console.log(res);
      this.stockInfo = res;
      this.stocksInfoPopulated = true;
      
    })

    this.restService.getPeer(this.tickerName).subscribe(res => {
      console.log(res);
      this.peers = res;
      this.peersPopulated = true;
    })

    this.restService.getFinanceData(this.tickerName, 1).subscribe(res => {
      
      this.priceStr = res.price_data[0][1].toFixed(2);
      this.priceStrPopulated = true;

    })

    this.restService.getFinanceData(this.tickerName, 10).subscribe(res => {

      for(let i of res.price_data){
          this.x_values.push(new Date(i[0]));
          this.y_values.push(i[1]);
      }
      console.log(this.x_values);
      console.log(this.y_values);

      for(var temp = 0; temp < this.x_values.length; temp++){
        this.dataPoints.push({x: this.x_values[temp], y: this.y_values[temp]})
        console.log(this.dataPoints[temp])
      }


      this.renderChart();

   
    })
  }else{


    this.compare_x_values = [];
    this.compare_y_values = [];
    this.data = [];
    this.compare_DataPoints = [];

    chart1.style.display = "none";
    chart2.style.display = "block";

      this.restService.getFinanceData(this.compare_TickerName, 10).subscribe(res => {

        for(let i of res.price_data){
            this.compare_x_values.push(new Date(i[0]));
            this.compare_y_values.push(i[1]);
        }
  
        for(var temp = 0; temp < this.compare_x_values.length; temp++){
          this.compare_DataPoints.push({x: this.compare_x_values[temp], y: this.compare_y_values[temp]})
          console.log(this.compare_DataPoints[temp])
        }
  
        this.renderCompareChart();
  
     
      })

    
    }

  }

  
  renderCompareChart(){
   

    let chart = new CanvasJS.Chart("chartContainer" + this.canvasChart, {
      zoomEnables: true,
      animationEnabled: true,
      exportEnabled: true,
      backgroundColor: null,
      responsize: true,
      title: {
        text: this.tickerName,
        titleFontColor: "white",
        labelFontColor:"white",
        fontColor:"white",
      },
      legend : {
        fontColor:"white",
      },
      toolTip:{
        fontColor: "#2D3C43",
      },
     axisX: {
      margin: 10,
      title: "Date",
      lineColor: "white",
      tickColor: "white",
      titleFontColor: "white",
      labelFontColor:"white",
     },
      axisY: {
        title: "Price",
        prefix: "$",
        margin: 10,
        lineColor: "white",
        tickColor: "white",
        titleFontColor: "white",
        labelFontColor:"white",
      },
      data: [
        {
        type: "area",
        color: "white",
        lineColor: "white",
        dataPoints: this.dataPoints
        },
        {
          type: "line",
          color: "#90129B",
          lineColor: "#90129B",
          name: this.compare_TickerName,
          showInLegend: true,
          dataPoints: this.compare_DataPoints
        }
      ]
    });

    chart.render();

    
  
  }


  renderChart(){
   

    let chart = new CanvasJS.Chart("chartContainer" + this.canvasChart, {
      zoomEnables: true,
      animationEnabled: true,
      exportEnabled: true,
      backgroundColor: null,
      responsize: true,
      title: {
        text: this.tickerName,
        titleFontColor: "white",
        labelFontColor:"white",
        fontColor:"white",
      },
     axisX: {
      margin: 10,
      title: "Date",
      lineColor: "white",
      tickColor: "white",
      titleFontColor: "white",
      labelFontColor:"white",
     },
      axisY: {
        title: "Price",
        prefix: "$",
        margin: 10,
        lineColor: "white",
        tickColor: "white",
        titleFontColor: "white",
        labelFontColor:"white",
      },
      data: [{
        type: "area",
        color: "white",
        lineColor: "#90129B",
        dataPoints: this.dataPoints
      }]
    });

    chart.render();

  }

 

}