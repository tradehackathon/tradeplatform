import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, interval, timer } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { Trade } from 'model/trade';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  constructor(private http: HttpClient) { }
  private apiURL = 'http://localhost:8080';

  getAll(): Observable<any> {
    return timer(10,2000).pipe(flatMap( () => {
      return this.http.get('//docker8.conygre.com:8080/trades');
    }));
  }


}
